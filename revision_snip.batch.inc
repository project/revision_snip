<?php

/**
 * @file
 * Batch Helpers for the Revision Snip module.
 */

/**
 * Implementation of callback_batch_finished() for the Revision Snipr.
 */
function revision_snip_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t("!count revisions were removed from the database.", array(
      '!count' => count($results),
    ));
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
  }
}

/**
 * Deletes groups of node revisions.
 *
 * Provides feedback in case the operation is being run in drush.
 *
 * @param array $vids
 *   An array of revision ids to delete.
 * @param int $complete_count
 *   The total number of revisions completed with this set.
 * @param int $total_count
 *   The total number of revisions being processed.
 * @param array $context
 *   Passed by association from the batch API.
 */
function revision_snip_node_batch_revision_delete(array $vids, $complete_count, $total_count, array &$context) {
  foreach ($vids as $vid) {
    node_revision_delete($vid);
  }
  $finished = round($complete_count / $total_count * 100, 3);
  // Batch in the browser already displays progress stats. We only want to add
  // message if we are running from drush.
  if (drupal_is_cli()) {
    $variables = array(
      '@progress' => $complete_count,
      '@max' => $total_count,
      '@percent' => $finished,
    );
    drush_print(dt('Removed @progress of @max revisions: @percent%...', $variables));
  }
}

CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

 INTRODUCTION
------------

The Revision Snip module provides a utility to prune node revisions that are
older than a certain number of days from the database. At some point in the 
lifetime of most Drupal 7 sites, revision storage becomes a source of database
bloat. This module provides a simple utility for site owners to be able to 
perform a customized one time pruning of these revisions through the GUI or via
drush.

The drush utilities optionally integrate with Drush Multi Processing, a plugin
allowing for multi-threaded drush commands. This is especially useful in sites
where revision bloat is exceptionally large.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/revision_snip

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/revision_snip

REQUIREMENTS
------------

This module doesn't require any contrib.

RECOMMENDED MODULES
-------------------

 * Drush Multi Processing (https://github.com/johnennewdeeson/drush-multi-processing):

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 
 * Configure user permissions in Administration » People » Permissions:

   - Use the administration pages and help (System module)

     The top-level administration categories require this permission to be
     accessible. The administration menu will be empty unless this permission
     is granted.

   - Administer Revision Snip

     Users in roles with the "Administer Revision Snip" permission will see
     the "Node revision snipping" at admin/config.

 * Customize the revision snip settings in Administration » Configuration »
   Development » Node revision snipping.

MAINTAINERS
-----------

Current maintainers:
 * Mike DeWolf (mrmikedewolf) - https://www.drupal.org/user/2679073

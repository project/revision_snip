<?php

/**
 * @file
 * Helpers for the Revision Snip module.
 */

/**
 * Generates numeric select options for various BH Prune admin settings forms.
 *
 * @param int $increment
 *   The increment by which to increase for each option.
 * @param int $max
 *   The maximum option value.
 *
 * @return array
 *   An array of numeric options suitable to serve as FAPI #options for a select
 *   element.
 */
function revision_snip_generate_numeric_options($increment, $max) {
  $options = array();
  $value = $increment;
  while ($value <= $max) {
    $options[$value] = number_format($value);
    $value += $increment;
  }
  return $options;
}

/**
 * Batch definition for the Revision Snipr.
 *
 * @param int $thread_id
 *   In cases of multithreading the thread id.
 * @param int $threads
 *   The total number of threads.
 *
 * @return array
 *   A batch definition suitable for batch_set().
 */
function revision_snip_batch_definition($thread_id = 0, $threads = 0) {
  $days = variable_get('revision_snip_age', REVISION_SNIP_DEFAULT_AGE);
  $limit = variable_get('revision_snip_max_records', REVISION_SNIP_DEFAULT_MAX_RECORDS);
  $chunk_size = variable_get('revision_snip_records_per_request', REVISION_SNIP_DEFAULT_RECORDS_PER_REQUEST);
  $batch_definition = array(
    'operations' => array(),
    'title' => t('Removing revisions older that @days days', array('@days' => $days)),
    'init_message' => t('Initializing revision pruning.'),
    'progress_message' => t('Removed @current of @total chunks.'),
    'error_message' => t('An error occurred while pruning revisions.'),
    'finished' => 'revision_snip_batch_finished',
    'file' => drupal_get_path('module', 'revision_snip') . '/revision_snip.batch.inc',
  );

  $timestamp_cutoff = REQUEST_TIME - ($days * 24 * 60 * 60);
  $revisions = db_query_range('SELECT nr.vid FROM {node_revision} AS nr LEFT JOIN {node} AS n ON nr.vid = n.vid WHERE nr.timestamp < :cutoff AND n.vid IS NULL', 0, $limit, array(
    ':cutoff' => $timestamp_cutoff,
  ))->fetchCol();
  $total_count = count($revisions);
  $revision_chunks = array_chunk($revisions, $chunk_size);
  // Handling for multithreading. This won't ever work without the
  // multithreading drush util.
  // @see https://www.deeson.co.uk/labs/multi-processing-part-1-how-make-drush-rush
  if (drupal_is_cli() && !empty($threads) && !empty($threads)) {
    $revision_jobs = array_chunk($revision_chunks, round(count($revision_chunks) / $threads));
    $total_count = round($total_count / $threads);
    $revision_chunks = $revision_jobs[$thread_id];
  }

  $complete_count = 0;
  foreach ($revision_chunks as $vids) {
    $complete_count += count($vids);
    $batch_definition['operations'][] = array(
      'revision_snip_node_batch_revision_delete',
      array(
        $vids, $complete_count, $total_count,
      ),
    );
  }
  return $batch_definition;
}

<?php

/**
 * @file
 * Drush commands for the Revision Snip module.
 */

/**
 * Implements hook_drush_command().
 */
function revision_snip_drush_command() {
  $items['revision-snip-team'] = array(
    'description' => 'A multithreader for revision-snip.',
    'aliases' => array('rst'),
    'options' => array(
      'threads' => 'The number of threads to use',
    ),
  );
  $items['revision-snip'] = array(
    'description' => dt("Prunes database revisions on nodes that are determined to be older than this module's settings"),
    'aliases' => array('rs'),
    'arguments' => array(
      'thread-id' => 'The name of this process, this will be the thread id',
      'threads' => 'The number of threads',
    ),
  );
  return $items;
}

/**
 * Callback for drush-bh-revision-mow.
 *
 * Run drush bh-revision-prune with multiple threads, if supported.
 *
 * @see https://www.deeson.co.uk/labs/multi-processing-part-1-how-make-drush-rush
 */
function drush_revision_snip_team() {
  if (!function_exists('drush_thread_manager')) {
    drush_print(dt("Multithreading isn't supported. @see https://www.deeson.co.uk/labs/multi-processing-part-1-how-make-drush-rush"));
    drupal_exit();
  }
  $threads = drush_get_option('threads', 1);
  drush_print("Going to work on {$limit} jobs with {$threads} threads...");
  // Normally we wouldn't use globals but this is how the creator of the module
  // instructed passing the number of threads (trying to be really "D7").
  $GLOBALS['revision_snip_threads'] = $threads;
  drush_thread_manager($threads, 1, $threads, '_revision_snip_team_setup');
}

/**
 * Setup for drush-bh-revision-mow.
 *
 * @param int $thread_id
 *   An identifier for the thread which will execute this command.
 * @param int $batch_size
 *   How many tasks this command should work on.
 * @param int $offset
 *   The position in a queue of jobs for the first job this command
 *   should work on.
 *
 * @return string
 *   A command which can be executed on the command line.
 */
function _revision_snip_team_setup($thread_id, $batch_size, $offset) {
  $threads = !empty($GLOBALS['revision_snip_threads']) ? $GLOBALS['revision_snip_threads'] : 0;
  return "drush revision-snip $thread_id $threads";
}

/**
 * Callback for drush bh-revision-prune.
 */
function drush_revision_snip($thread_id = 0, $threads = 0) {
  module_load_include('inc', 'revision_snip');
  $batch_definition = revision_snip_batch_definition($thread_id, $threads);
  batch_set($batch_definition);
  drush_backend_batch_process();
}

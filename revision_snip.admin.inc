<?php

/**
 * @file
 * Admin forms for the Revision Snip module.
 */

/**
 * Settings form for the Revision Snip module.
 */
function revision_snip_settings($form, &$form_state) {
  module_load_include('inc', 'revision_snip');
  $form['pruning'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pruning'),
  );
  $form['pruning']['revision_snip_age'] = array(
    '#type' => 'select',
    '#title' => t('Max revision age'),
    '#description' => t('The maximum number of days to keep a revision in the system'),
    '#options' => revision_snip_generate_numeric_options(5, 50),
    '#default_value' => variable_get('revision_snip_age', REVISION_SNIP_DEFAULT_AGE),
  );
  $form['pruning']['revision_snip_max_records'] = array(
    '#type' => 'select',
    '#title' => t('Max records'),
    '#description' => t('The maximum number of records to remove during an operation. Higher values have a higher chance of producing errors.'),
    '#options' => revision_snip_generate_numeric_options(50000, 500000),
    '#default_value' => variable_get('revision_snip_max_records', REVISION_SNIP_DEFAULT_MAX_RECORDS),
  );
  $form['pruning']['revision_snip_records_per_request'] = array(
    '#type' => 'select',
    '#title' => t('Max records'),
    '#description' => t('The maximum number of records to remove during a single request. Higher values will speed up the operation overall but also have a higher chance of producing errors.'),
    '#options' => revision_snip_generate_numeric_options(5, 50),
    '#default_value' => variable_get('revision_snip_records_per_request', REVISION_SNIP_DEFAULT_RECORDS_PER_REQUEST),
  );
  return system_settings_form($form);
}

/**
 * Confirm form to prune revisions.
 */
function revision_snip_confirm($form, &$form_state) {
  $question = t('Are you sure you want to remove revisions older that @days days?', array(
    '@days' => variable_get('revision_snip_age', REVISION_SNIP_DEFAULT_AGE),
  ));
  $description = t('These revisions will be irretrievable after this action is complete. Save a database backup first to keep them.');
  return confirm_form($form, $question, 'admin/config/revision-prune', $description, t('Prune revisions'), t('Not now'), 'revision_snip_confirm');
}

/**
 * Submit handler for the revision prune confirm form.
 */
function revision_snip_confirm_submit($form, &$form_state) {
  module_load_include('inc', 'revision_snip');
  $batch_definition = revision_snip_batch_definition();
  batch_set($batch_definition);
}
